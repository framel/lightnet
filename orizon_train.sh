#! /bin/sh

clear
echo "               Welcome in"

banner () {
echo "
 _______  _______ _________ _______  _______  _       
(  ___  )(  ____ )\__   __// ___   )(  ___  )( (    /|
| (   ) || (    )|   ) (   \/   )  || (   ) ||  \  ( |
| |   | || (____)|   | |       /   )| |   | ||   \ | |
| |   | ||     __)   | |      /   / | |   | || (\ \) |
| |   | || (\ (      | |     /   /  | |   | || | \   |
| (___) || ) \ \_____) (___ /   (_/\| (___) || )  \  |
(_______)|/   \__/\_______/(_______/(_______)|/    )_)

         _________ _______ _________ _______  _       
|\     /|\__   __// ___   )\__   __/(  ___  )( (    /|
| )   ( |   ) (   \/   )  |   ) (   | (   ) ||  \  ( |
| |   | |   | |       /   )   | |   | |   | ||   \ | |
( (   ) )   | |      /   /    | |   | |   | || (\ \) |
 \ \_/ /    | |     /   /     | |   | |   | || | \   |
  \   /  ___) (___ /   (_/\___) (___| (___) || )  \  |
   \_/   \_______/(_______/\_______/(_______)|/    )_)
"
}

#######################################################
#                    CONSTANTES                       #
#######################################################

CONFIGDIR=.config
DEFAULTWEIGHT=default.weight
DEFAULTTHRESH=20
DEFAULTPROP=10


yesno ()
{
	if [ "$1" = "yes" -o "$1" = "y" ]
	then
		echo YES
		return 0
	elif [ "$1" = "no" -o "$1" = "n" ]
	then
		echo NO
		return 1
	else
		echo "Answer 'yes' or 'no'"
		read answer
		yesno $answer
	fi
}

ispercent ()
{
	if [ $1 -ge 0 ] && [ $1 -le 100 ]
		then
		return 0
	else
		return 1
	fi
}

naming()
{
	if [ -f "$1.names" ]
	then
		echo "$1 already exist. Overwrite it?"
		read answer
		yesno $answer
		if [ "$?" = 0 ]
		then
			rm $1.names
		fi
	fi
	echo "Write your classes here Type 'END' when finish"
	touch $1.names
	names=0
	while [ "$names" != "END" ]
	do
		read names
		if [ "$names" != "END" ]
		then
			echo $names >> $1.names
		fi
	done
	echo "**** names file ****"
	cat $1.names
	echo "********************"
}

process ()
{
	train=\'$1_train.txt\'
	test=\'$1_test.txt\'
	echo "Percentage test [default '$DEFAULTPROP']: \c"
	read percent_test
	ispercent $percent_test 2> /dev/null
	if [ $? -eq 1 ]
		then
		percent_test="$DEFAULTPROP"
	fi
	echo "#!/usr/local/bin/python3

import glob, os

# Current directory
current_dir = os.path.dirname(os.path.abspath(__file__))

# Directory where the data will reside, relative to 'darknet.exe'
path_data = 'data/obj/'

# Percentage of images to be used for the test set
percentage_test = $percent_test;

# Create and/or truncate train.txt and test.txt
file_train = open($train, 'w')
file_test = open($test, 'w')

# Populate train.txt and test.txt
counter = 1
index_test = round(100 / percentage_test)
for pathAndFilename in glob.iglob(os.path.join(current_dir, \"*.jpg\")):
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))
    if counter == index_test:
        counter = 1
        file_test.write(path_data + title + '.jpg' + \"\n\")
    else:
        file_train.write(path_data + title + '.jpg' + \"\n\")
        counter = counter + 1" > $1_process.py
}

train ()
{
	echo " Training
"
	echo "Project name:"
	read project_name
	echo "Images path:"
#	read img_path
	echo "./darknet detector train $project_name.data $project_name.cfg"
	echo "Generate a .name file? yes/no"
	read answer
	yesno $answer
	if [ $? -eq 0 ]
	then
		naming $project_name
	fi
	n_classes="$(cat $project_name.names | wc -l)"
	echo "You have $n_classes classes"
	echo "Generate python script"
	process $project_name
	make_config $project_name $n_classes
}

detect ()
{
	echo "Detector
"
}

generate() {
	if [ "$1" = "" ]
	then
		make_config default
	else
		make_config $1
	fi
}

parse()
{
	if [ "$1" = "help" ]
	then
		help $2
	elif [ "$1" = "quit" ]
	then
		echo "bye"
		exit
	elif [ "$1" = "train" ]
	then
			train
	elif [ "$1" = "detect" ]
	then
		detect
	elif [ "$1" = "clear" ]
	then
		clear
	elif [ "$1" = "banner" ]
	then
		banner
	elif [ "$1" = "config" ]
	then
		config $2
	elif [ "$1" = "gen" ]
	then
		generate $2
	elif [ "$1" = "clean" ]
	then
		clean_project $2
	else
		echo "Unknow command"
	fi
}

repl()
{
	echo "orizon> \c"
	read cmd1 cmd2
	parse $cmd1 $cmd2
}

clean_project ()
{
	if [ "$1" = "" ]
	then
		help_clean
		return
	fi
	/bin/rm -rf $1.cfg
	/bin/rm -rf $1_train.txt
	/bin/rm -rf $1_test.txt
	/bin/rm -rf $1_process.py
	/bin/rm -rf $1.names
}

help_clean ()
{
	echo "usage: clean <project name>

The 'clean' command suppress all the files releated to a project.
Use with caution..."
}

config_thresh () {
	echo "Threshold purcentage [default '$DEFAULTTHRESH']: \c"
	read thresh
	ispercent $thresh 2> /dev/null
	if [ $? = 0 ]
	then
		return
	elif [ "$thresh" = "" ]
	then
		echo "Thresh set to default."
		thresh="$DEFAULTTHRESH"
		return
	fi
	echo "Integer between 0 and 100."
	config_thresh
}

config_weight () {
	echo "Weight file [default '$DEFAULTWEIGHT']: \c"
	read weight
	if [ -f "$weight" ]
	then
		return
	elif [ "$weight" = "" ]
	then
		echo "Weight file set to default"
		weight="$DEFAULTWEIGHT"
		return
	fi
	echo "File doesn't exist."
	config_weight	
}

config_detect () {
	mkdir -p $CONFIGDIR
	config_thresh
	config_weight
	echo "thresh=$thresh
weight=$weight" > $CONFIGDIR/detect.cfg
}

config_train () {
echo "config train"
}

config () {
	if [ "$1" = "detect" ]
	then
		config_detect
	elif [ "$1" = "train" ]
	then
		config_train
	else
		echo "usage: config <module>"
	fi
}

help_config () {
	echo "usage: config <module>

The 'config' command allow to set parameters for the modules.

Available modules: 
==================

* detect
* train
"
}

details () {
	if [ "$1" = "clean" ]
	then
		help_clean
	elif [ "$1" = "config" ]
	then
		help_config
	else
		echo "No available he for the '$1' command."
	fi
}

help() {
	if [ "$#" -eq 0 ]
	then
		echo "        ORIZON VIZION

Commands:

help    Show this help
quit    Leave this program
train   Train neural network
detect  Launch detect for some object
clean   Drop a project
clear   Clear the screen
config  Configure the settings
gen     Generate models

Usage: help [command] for more details
"
	else
		details $1
	fi
	
}

make_config () {
echo "[net]
# Testing
batch=1
subdivisions=1
# Training
# batch=64
# subdivisions=8
height=416
width=416
channels=3
momentum=0.9
decay=0.0005
angle=0
saturation = 1.5
exposure = 1.5
hue=.1

learning_rate=0.001
burn_in=1000
max_batches = 80200
policy=steps
steps=40000,60000
scales=.1,.1

[convolutional]
batch_normalize=1
filters=32
size=3
stride=1
pad=1
activation=leaky

[maxpool]
size=2
stride=2

[convolutional]
batch_normalize=1
filters=64
size=3
stride=1
pad=1
activation=leaky

[maxpool]
size=2
stride=2

[convolutional]
batch_normalize=1
filters=128
size=3
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=64
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=128
size=3
stride=1
pad=1
activation=leaky

[maxpool]
size=2
stride=2

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[maxpool]
size=2
stride=2

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[maxpool]
size=2
stride=2

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=1
pad=1
activation=leaky


#######

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=1024
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=1024
activation=leaky

[route]
layers=-9

[convolutional]
batch_normalize=1
size=1
stride=1
pad=1
filters=64
activation=leaky

[reorg]
stride=2

[route]
layers=-1,-4

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=1024
activation=leaky

[convolutional]
size=1
stride=1
pad=1
filters=$((($2+5)*3))
activation=linear


[region]
anchors =  1.3221, 1.73145, 3.19275, 4.00944, 5.05587, 8.09892, 9.47112, 4.84053, 11.2364, 10.0071
bias_match=1
classes=$2
coords=4
num=5
softmax=1
jitter=.3
rescore=1

object_scale=5
noobject_scale=1
class_scale=1
coord_scale=1

absolute=1
thresh = .6
random=1" > $1.cfg
}

banner
echo "Type 'help' for some help !"
echo

while [ 1 -eq 1 ]
do
	repl
done
